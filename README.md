# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://bitbucket.org/asistent/stroboskop
cd stroboskop/
git add jscolor.js
git rm --cached nepotrebno.js
git commit -m "Priprava potrebnih JavaScript knjižnic"
git push origin master
```

Naloga 6.2.3:
https://bitbucket.org/lb2448/stroboskop/commits/60d8a708b0eaf502b8279d43b195318de1d7ba96

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/lb2448/stroboskop/commits/1822839a7e8944f20621d074509b913b4112253f

Naloga 6.3.2:
https://bitbucket.org/lb2448/stroboskop/commits/b70ca62f76f88c1768e71e305878a7529fc58855

Naloga 6.3.3:
https://bitbucket.org/lb2448/stroboskop/commits/f7e03aa784d6cda882457c02c6e6cbe9c56e915f

Naloga 6.3.4:
https://bitbucket.org/lb2448/stroboskop/commits/1735f722610f734d40d05451d020cb7a6b9f6eb0

Naloga 6.3.5:

```
git checkout -b izgled
git commit -am "Dodajanje stilov spletne strani"
git push origin izgled

git commit -am "Desna poravnava besedil"
git push origin izgled

git commit -am "Dodajanje gumba za dodajanje barv"
git push origin izgled

git commit -am "Dodajanje robov in senc"
git push origin izgled

git checkout master
git merge --no-ff izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/lb2448/stroboskop/commits/8679cfef98772915535c6e88aa1ef70ce6c8a4e5

Naloga 6.4.2:
https://bitbucket.org/lb2448/stroboskop/commits/ef879439fa7509875926ae74adb73cae0ebce24a

Naloga 6.4.3:
https://bitbucket.org/lb2448/stroboskop/commits/2bd47686c37154e4be5f2d15d9fe621a64828276

Naloga 6.4.4:
https://bitbucket.org/lb2448/stroboskop/commits/4a87d12d699445ee5fa935293a2430e9b8b9da98